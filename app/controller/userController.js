const { default: mongoose } = require("mongoose")
const userModel = require("../models/userModel")

// lấy danh sách user
const getAllUsers = (req, res) => {

    userModel.find()
        .then((data) => {
            return res.status(201).json({
                message: "Get all User sucessfully",
                data: data
            })
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message

        })
        )
}

// get User By Id
const getUserById = (req, res) => {
    // B1: thu thập dữ liệu
    let id = req.params.userId
    // B2:validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    // B3: thao tác CSDL
    userModel.findById(id)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Get User By Id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message

        })
        )
}

// Create User
const createUser = (req, res) => {
    let body = req.body
    if (!body.fullName) {
        return res.status(400).json({
            message: `fullName is required`
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: `phone is invalid`
        })
    }
    let newUser = {
        _id: new mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        status: body.status
    }

    userModel.create(newUser)
        .then(data => res.status(200).json({
            message: "Create User successfully",
            data
        }))
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}

// Update User 
const updateUser = (req, res) => {
    let id = req.params.userId
    let body = req.body

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!body.fullName) {
        return res.status(400).json({
            message: `fullName is required`
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: `phone is invalid`
        })
    }
    let updateUser = {

        fullName: body.fullName,
        phone: body.phone,
        status: body.status
    }

    userModel.findByIdAndUpdate(id, updateUser)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Update User sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}
// Delete User 

const deleteUser = (req, res) => {
    // B1: thu thập dữ liệu
    let id = req.params.userId
    // B2:validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    // B3: thao tác CSDL
    userModel.findByIdAndDelete(id)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Delete User sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}


module.exports = { getAllUsers, createUser, getUserById, updateUser, deleteUser }