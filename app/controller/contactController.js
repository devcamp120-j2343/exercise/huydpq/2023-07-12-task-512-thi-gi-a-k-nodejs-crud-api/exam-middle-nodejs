const { default: mongoose } = require("mongoose");
const contactModel = require("../models/contactModel");

// get all contact
const getAllContact = (req, res) => {
    contactModel.find()
        .then((data) => {
            return res.status(201).json({
                message: "Get all Contact sucessfully",
                courses: data
            })
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message

        })
        )
}
// get Contact By Id
const getContactById = (req, res) => {
    // B1: thu thập dữ liệu
    let id = req.params.contactId
    // B2:validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    // B3: thao tác CSDL
    contactModel.findById(id)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Get Contact By Id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message

        })
        )
}
// create Contact
const createContact = (req, res) => {
    let body = req.body;

    if (!body.email || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(body.email))) {
        return res.status(400).json({
            message: `email is required`
        })
    }
    let newContact = {
        _id: new mongoose.Types.ObjectId(),
       email: body.email
    }

    contactModel.create(newContact)
        .then(data => res.status(200).json({
            message: "Create Contact successfully",
            data
        }))
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}

// Update Contact 
const updateContact = (req, res) => {
    let id = req.params.contactId
    let body = req.body

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!body.email || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(body.email))) {
        return res.status(400).json({
            message: `email is required`
        })
    }
    let updateContact = {
        email: body.email,
       
    }

    contactModel.findByIdAndUpdate(id, updateContact)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Update Contact sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}
// Delete Contact 
const deleteContact = (req, res) => {
    // B1: thu thập dữ liệu
    let id = req.params.contactId
    // B2:validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    // B3: thao tác CSDL
    contactModel.findByIdAndDelete(id)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Delete Contact sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}

module.exports = { createContact, getAllContact, getContactById, updateContact, deleteContact }