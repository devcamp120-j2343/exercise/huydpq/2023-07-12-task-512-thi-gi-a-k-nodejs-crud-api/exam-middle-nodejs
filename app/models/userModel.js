// Khai báo mongoose
const mongoose = require('mongoose')

// B2: Khai báo Schema
const Schema = mongoose.Schema

// B3: khai báo các thuộc tính của đối tượng 
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    status: {
        type: String,
        default: 'Level 0'
    }
}, {
    timestamps: true
}
)

// B4: export Schema để sử dụng
module.exports = mongoose.model('user', userSchema)