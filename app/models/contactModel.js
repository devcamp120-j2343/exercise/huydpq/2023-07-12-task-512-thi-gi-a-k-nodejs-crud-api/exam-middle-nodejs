// Khai báo mongoose
const mongoose = require('mongoose')

// B2: Khai báo Schema
const Schema = mongoose.Schema

// B3: khai báo các thuộc tính của đối tượng 
const contactSchema = new Schema({ 
    _id: mongoose.Types.ObjectId,
    email: {
        type: String,
        required: true
    }
}, {
    timestamps: true
}
)

// B4: export Schema để sử dụng
module.exports = mongoose.model('contact', contactSchema)