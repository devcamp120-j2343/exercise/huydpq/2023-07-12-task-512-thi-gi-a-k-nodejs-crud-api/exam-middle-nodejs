const express = require('express');
const { getAllUsers, createUser, getUserById, updateUser, deleteUser } = require('../controller/userController');

const router = express.Router();

// get all User
router.get("/",getAllUsers)

//create User
router.post("/",createUser)

//get User ById
router.get("/:userId",getUserById)

//Update User ById
router.put("/:userId", updateUser)

//Delete User ById
router.delete("/:userId", deleteUser)

module.exports = router