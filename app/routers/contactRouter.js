const express = require('express');
const { createContact, getAllContact, getContactById, updateContact, deleteContact } = require('../controller/contactController');


const router = express.Router();

// get all User
router.get("/",getAllContact)

//create User
router.post("/",createContact)

//get User ById
router.get("/:contactId",getContactById)

//Update User ById
router.put("/:contactId", updateContact)

//Delete User ById
router.delete("/:contactId", deleteContact )

module.exports = router