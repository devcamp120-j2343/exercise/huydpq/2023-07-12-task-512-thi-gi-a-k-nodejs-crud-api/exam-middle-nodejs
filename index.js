const express = require('express')
const app = express()
const port = 8000
// Khai báo Monolithic
const path = require('path')

app.use(express.json())
// Khai báo mongoose
const mongoose = require('mongoose')
// Kết nối mongoDB 
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Vaccination')
    .then(() => console.log('Successfully connected to MongoDB')) 
    .catch(err => console.log(err))

// Tạo các Model từ Schema =>Tạo collection trên MongoDB
const userModel = require('./app/models/userModel')
const contactModel = require('./app/models/contactModel')

// khai báo router
const  userRouter  = require('./app/routers/userRouter')
const  contactRouter  = require('./app/routers/contactRouter')
const { url } = require('inspector')

//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
    var today = new Date();
    console.log("Current time: ", today);

    next();
});

//Middleware in ra console request url mỗi khi API gọi
app.use((request, response, next) => {
    console.log("Method: ", request.url);
    next();
});

app.use('/user', userRouter)
app.use('/contact', contactRouter)

//Monolithic -hiển thị ảnh và load page
app.use('/', express.static(__dirname + "/views/pages"))
app.get('/', (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/pages/index.html"))
})

// app.use('/admin/users', express.static(__dirname + "/views/admin"))
// app.get('/admin/users', (req, res) => {
//     console.log(__dirname);
//     res.sendFile(path.join(__dirname + "/views/admin/users.html"))
// })
// app.use('/admin/contacts', express.static(__dirname + "/views/admin"))
// app.get('/admin/contacts', (req, res) => {
//     console.log(__dirname);
//     res.sendFile(path.join(__dirname + "/views/admin/contacts.html"))
// })

app.listen(port, () => console.log(`App listening at ${port}`)) 