import { gCONTACT_URL, successModalShow, warningModalShow } from "./constants.js"

const createContact = async () => {
    // B1: thu thập dữ liệu
    let vContact = {
        email: $('#inp-email').val()
    }

    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(vContact.email))) {
        warningModalShow('Please fill your correct email')
    } else {
        // B3: callAPI để thêm mới contact
        fetch(gCONTACT_URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(vContact)
        })
            .then(res => res.json())
            .then(data => successModalShow('Successfully created your email'))
    }
}

export default createContact