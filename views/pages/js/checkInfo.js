import { gUSER_URL, warningModalShow } from "./constants.js"

const checkInfo = () => {
    // B1: thu thập dữ liệu
    let checkInfo = {
        fullName: $('#input-name').val(),
        phone: $('#input-cus-number').val()
    }
    // B2: validate dữ liệu
    if (validateData(checkInfo)) {
        // B3: call API để check
        callAPIAllUser(checkInfo)
    }

}
// B2: validate dữ liệu
 const validateData = (paramInfo) => {
    if (paramInfo.fullName == '') {
        warningModalShow('Please fill your full name')
        return false
    }
    if (!(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(paramInfo.phone))) {
        warningModalShow('Please fill your correct phone')
        return false
    }
    return true
}
// B3: call API để check thông tin patient
const callAPIAllUser = async (paramInfo) => {
    let callApi = await fetch(gUSER_URL)
    let data = await callApi.json()
    if(data ){
        handleData(data.data, paramInfo)
    }
}

// B4: xử lý dữ liệu trả về (Render thông tin ra giao diện)
const handleData = (paramData, paramInfo) => {
    let vInfoFiltered = paramData.filter(item => item.phone === paramInfo.phone && item.fullName === paramInfo.fullName)
    if (vInfoFiltered.length > 0) {
        $('#div-checked').html('')
        $('#div-checked').append(divResult(vInfoFiltered[0]))
    } else {
        $('#div-checked').html('')
        $('#div-checked').append('No infomation!')
    }
}

const divResult = (paramInfo) => {
    return (`
        <p>Id User: ${paramInfo._id}</p> <br>
        <p>Full Name: ${paramInfo.fullName}</p> <br>
        <p>Phone: ${paramInfo.phone}</p> <br>
        <p>Status: ${paramInfo.status}</p> <br>
    `)
}
export {checkInfo, validateData} 