const gCONTACT_URL = 'http://localhost:8000/contact'
const gUSER_URL = 'http://localhost:8000/user'

// MODAL WARNING
const warningModal = (paramAlert) => {
    return (`
    <div id="warning-user-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content bg-warning">
                <div class="modal-header">
                    <h3 class="modal-title" id="h5-modal-title">Warning</h3>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">${paramAlert}</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-warning-confirm" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
`)
}

// MODAL SUCCESS
const successModal = (paramAlert) => {
    return (`
    <div id="success-user-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h3 class="modal-title" id="h5-modal-title">Success</h3>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">${paramAlert}</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-success-confirm">OK</button>
                </div>
            </div>
        </div>
    </div>
`)
}

//hàm hiện cảnh báo khi không thỏa vaildate
const warningModalShow = (paramAlert) => {
    $('#warning-modal').empty();
    $('#warning-modal').append(warningModal(paramAlert));
    $('#warning-user-modal').modal('show');
}

//hàm hiện thông báo thành công khi không thỏa vaildate
const successModalShow = (paramAlert) => {
    $('#success-modal').empty();
    $('#success-modal').append(successModal(paramAlert));
    $('#success-user-modal').modal('show');
    $('#success-modal').on('click', '.btn-success-confirm', () => {
        location.reload()
    })
}

// 

export { gCONTACT_URL, gUSER_URL, warningModalShow, successModalShow }