import {checkInfo} from "./checkInfo.js"
import createContact from "./createEmail.js"
import { createInfo, verifyPhoneNumber } from "./createInfo.js"


const start = () => {
    // Gán sự kiện nút check info
    $('#btn-check').on('click', event => {
        event.preventDefault()
        checkInfo()
    })

    // Gán sự kiện nút verify số điện thoại
    $('#btn-patient-verify').on('click', event => {
        event.preventDefault()
        verifyPhoneNumber()
    })

    // Gán sự kiện nút submit patient info
    $('#btn-patient-submit').on('click', event => {
        event.preventDefault()
        createInfo()
    })

    // Gán sự kiện nút submit contact email
    $('#btn-email-submit').on('click', event => {
        event.preventDefault()
        createContact()
    })
}
start()