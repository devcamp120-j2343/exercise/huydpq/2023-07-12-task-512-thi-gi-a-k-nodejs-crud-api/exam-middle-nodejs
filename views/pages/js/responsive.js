$(document).ready(function () {

    if ($('body').innerWidth() <= 960)
        $(".submit-schedule-button").appendTo(".submit-schedule > div");

    if ($('body').innerWidth() <= 1180) {
        $(".vaccine-regis-img").prependTo(".vaccine-regis > div.row");
        $(".vaccine-regis h1").prependTo(".vaccine-regis");
    }
})