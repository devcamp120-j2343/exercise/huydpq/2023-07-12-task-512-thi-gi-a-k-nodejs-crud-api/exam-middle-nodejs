import { gUSER_URL, successModalShow, warningModalShow } from "./constants.js";
import { validateData } from "./checkInfo.js";


const createInfo = async () => {
    // B1: thu thập dữ liệu
    let vNewUser = getNewUser()
    // B2: validate dữ liệu
    if (validateData(vNewUser) && await callAPIToCheck(vNewUser)) {

        // B3: call API để thêm thông tin 
        callAPIToCreateUser(vNewUser)
    }
}
// B1: thu thập dữ liệu
const getNewUser = () => {
    return (
        {
            fullName: $('#inp-patient-name').val(),
            phone: $('#inp-patient-number').val(),
            status: 'Level 0'
        }
    )
}

const callAPIToCheck = async (paramInfo) => {
    let callApi = await fetch(gUSER_URL)
    let data = await callApi.json()

    let filterData = data.data.filter(item => {
        return item.phone === paramInfo.phone
    })

    if (filterData.length > 0) {
        warningModalShow('Số điện thoại đã được đăng ký')
        return false
    }
    return true
}
// B3: call API để thêm thông tin 
const callAPIToCreateUser = (paramInfo) => {
    console.log(paramInfo)
    fetch(gUSER_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(paramInfo)
    })
        .then(res => res.json())
        .then(data => handleCreatePatient(data))
        .catch(err => console.log(err))
}


const handleCreatePatient = (paramData) => {
    console.log(paramData);
    successModalShow('Successfully created new infomation')
}

// verify sdt
const verifyPhoneNumber = () => {
    // /(84|0[3|5|7|8|9])+([0-9]{8})\b/g
    if (!(/(0[3|5|7|8|9])+([0-9]{8})\b/g.test($('#inp-patient-number').val()))) {
        warningModalShow('Số điện thoại không đúng định dạng')
    } else {
        warningModalShow('Số điện thoại hợp lệ')

    }
}
export { createInfo, verifyPhoneNumber }