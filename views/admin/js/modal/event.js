/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

import { gUSER_URL, gId } from "../constants.js";
import { warningModalShow, successModalShow } from "./modal.js";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm xử lý sự kiện ấn nút thêm mớiUser trên modal
const onBtnCreateUser = () => {
    //B1: thu thập dữ liệu
    var vUserObj;
    vUserObj = getNewUser()
    //B2:validate dữ liệu
    if (validateUser(vUserObj)) {
        $('#warning-modal').empty()
        console.log(vUserObj);
        //B3: call api để thêm mớiUser
        callAPIToCreateUser(vUserObj)
    }
}

//hàm xử lý sự kiện ấn nút sửaUser trên modal
const onBtnUpdateUser = () => {
    console.log(gId);
    //B1: thu thập thông tin và điền vào input
    var vUserObj = getUpdateUser()
    console.log(vUserObj);
    if (validateUser(vUserObj)) {
        callAPIToUpdateUser(vUserObj)
    }
}

//hàm xử lý sự kiện ấn nút xóa User trên modal
const onBtnDeleteUser = () => {
    $.ajax({
        url: gUSER_URL + '/' + gId,
        type: 'DELETE',
        success: () => successModalShow('Delete User Sucessful'),
        error: err => console.log(err)
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// B1: hàm thu thập thông tin fill vào input của xem chi tiếtUser
const getUpdateUser = () => {
    return (
        {
            fullName: $('#inp-name').val(),
            phone: $('#inp-phone').val(),
            status: $('#select-status').val()
        }
    )
}

//B1: hàm thu thập thông tin tạo mớiUser
const getNewUser = () => {
    return (
        {
            fullName: $('#inp-new-name').val(),
            phone: $('#inp-new-phone').val(),
            status: $('#select-new-status').val()
        }
    )
}

//B2: hàm validate thông tin tạo mới từUser
const validateUser = (paramUser) => {
    if (paramUser.fullName == '') {
        warningModalShow(`Please fill your full name`)
        return false
    }
    if (paramUser.phone == '') {
        warningModalShow(`Please fill yourUser's description more than 20 characters`)
        return false
    }
    if (paramUser.status == '0') {
        warningModalShow(`Please choose your status`)
        return false
    }
    return true
}

//B3: call api để sửaUser
const callAPIToUpdateUser = (paramUser) => {
    $.ajax({
        url: gUSER_URL + '/' + gId,
        type: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramUser),
        success: () => successModalShow('Update User Successful'),
        error: err => console.log(err)
    })
}

//B3: call api để thêm mớiUser
const callAPIToCreateUser = (paramUser) => {
    $.ajax({
        url: gUSER_URL,
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramUser),
        success: () => successModalShow('Create New User Successful'),
        error: err => console.log(err)
    })
}


export { onBtnCreateUser, onBtnDeleteUser, onBtnUpdateUser }