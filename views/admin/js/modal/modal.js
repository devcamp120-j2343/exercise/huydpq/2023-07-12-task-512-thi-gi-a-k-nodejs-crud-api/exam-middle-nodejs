
const addModalToHtml = () => {
    $('#create-modal').append(createModal())
    $('#update-modal').append(updateModal())
    $('#delete-modal').append(deleteModal())
}

// <!-- Create voucher modal -->
const createModal = () => {
    return (`
    <div id="create-user-modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New User</h5>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body d-flex align-items-center">
                <form class="col-md-12">
                    <div class="row my-3">
                        <label class="col-md-5">Full Name</label>
                        <input type="text" id="inp-new-name" placeholder="Enter Full Name..."
                            class="col-md-7 form-control">
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Phone</label>
                        <input type="text" id="inp-new-phone" placeholder="Enter Phone..."
                            class="col-md-7 form-control">
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Status</label>
                        <select id="select-new-status" class="col-md-7 form-control">
                            <option value="0" selected> Choose Status</option>
                            <option value="Level 0" >Level 0</option>
                            <option value="Level 1" >Level 1</option>
                            <option value="Level 2" >Level 2</option>
                            <option value="Level 3" >Level 3</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn-create-user">Confirm</button>
                <button class="btn btn-secondary" id="btn-cancel-create-user" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
    `)
}

// Update Modal
const updateModal = () => {
    return (`
    <div id="update-user-modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">user Detail</h5>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body d-flex align-items-center">
                <form class="col-md-12">
                    <div class="row my-3">
                        <label class="col-md-5">Full Name</label>
                        <input type="text" id="inp-name" placeholder="Enter Full Name..."
                            class="col-md-7 form-control">
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Phone</label>
                        <input type="text" id="inp-phone" placeholder="Enter Phone..."
                            class="col-md-7 form-control">
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Status</label>
                        <select id="select-status" class="col-md-7 form-control">
                            <option value="0" selected> Choose Status</option>
                            <option value="Level 0" >Level 0</option>
                            <option value="Level 1" >Level 1</option>
                            <option value="Level 2" >Level 2</option>
                            <option value="Level 3" >Level 3</option>
                        </select>
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Created At</label>
                        <input type="text" id="inp-created-at" placeholder="Created At..."
                            class="col-md-7 form-control" disabled>
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Updated At</label>
                        <input type="text" id="inp-updated-at" placeholder="Updated At..."
                            class="col-md-7 form-control" disabled>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn-update-user">Confirm</button>
                <button class="btn btn-secondary" id="btn-cancel-update-user" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
    `)
}

//  <!-- Delete confirm modal -->
const deleteModal = () => {
    return (`
    <div class="modal fade" tabindex="-1" id="delete-user-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h5-modal-title">User Delete Confirmation</h5>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label>Bạn có chắc chắn muốn xóa thông tin liên hệ này không?</label>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="btn-delete-user">Confirm</button>
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
`)
}


//hàm hiển thị cảnh báo khi không thỏa điều kiện validate dùng modal
const warningModal = (paramAlert) => {
    return (`
    <div id="warning-user-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content bg-warning">
                <div class="modal-header">
                    <h3 class="modal-title" id="h5-modal-title">Warning</h3>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">${paramAlert}</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
`)
}

//hàm hiển thị cảnh báo khi không thỏa điều kiện validate dùng modal
const successModal = (paramAlert) => {
    return (`
    <div id="success-user-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h3 class="modal-title" id="h5-modal-title">Success</h3>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">${paramAlert}</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-success-confirm">OK</button>
                </div>
            </div>
        </div>
    </div>
`)
}

//hàm hiện cảnh báo khi không thỏa vaildate
export const warningModalShow = (paramAlert) => {
    $('#warning-modal').empty()
    $('#warning-modal').append(warningModal(paramAlert))
    $('#warning-user-modal').modal('show')
}

//hàm hiện thông báo thành công khi không thỏa vaildate
export const successModalShow = (paramAlert) => {
    $('#success-modal').empty()
    $('#success-modal').append(successModal(paramAlert))
    $('#success-modal').on('click', '.btn-success-confirm', () => {
        location.reload()
    })
    $('#success-user-modal').modal('show')
}



export default addModalToHtml