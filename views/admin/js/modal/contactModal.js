
const addContactModalToHtml = () => {
    $('#create-modal').append(createModal())
    $('#update-modal').append(updateModal())
    $('#delete-modal').append(deleteModal())
}

// <!-- Create voucher modal -->
const createModal = () => {
    return (`
    <div id="create-contact-modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Contact</h5>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body d-flex align-items-center">
                <form class="col-md-12">
                    <div class="row my-3">
                        <label class="col-md-5">Email</label>
                        <input type="text" id="inp-new-email" placeholder="Enter Email..."
                            class="col-md-7 form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn-create-contact">Confirm</button>
                <button class="btn btn-secondary" id="btn-cancel-create-contact" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
    `)
}

// Update Modal
const updateModal = () => {
    return (`
    <div id="update-contact-modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">user Detail</h5>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body d-flex align-items-center">
                <form class="col-md-12">
                    <div class="row my-3">
                        <label class="col-md-5">Email</label>
                        <input type="text" id="inp-email" placeholder="Enter Email..."
                            class="col-md-7 form-control">
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Created At</label>
                        <input type="text" id="inp-created-at" placeholder="Created At..."
                            class="col-md-7 form-control" disabled>
                    </div>
                    <div class="row my-3">
                        <label class="col-md-5">Updated At</label>
                        <input type="text" id="inp-updated-at" placeholder="Updated At..."
                            class="col-md-7 form-control" disabled>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btn-update-contact">Confirm</button>
                <button class="btn btn-secondary" id="btn-cancel-update-contact" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
    `)
}

//  <!-- Delete confirm modal -->
const deleteModal = () => {
    return (`
    <div class="modal fade" tabindex="-1" id="delete-contact-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="h5-modal-title">Contact Delete Confirmation</h5>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label>Bạn có chắc chắn muốn xóa thông tin liên hệ này không?</label>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="btn-delete-contact">Confirm</button>
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
`)
}


//hàm hiển thị cảnh báo khi không thỏa điều kiện validate dùng modal
const warningModal = (paramAlert) => {
    return (`
    <div id="warning-contact-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content bg-warning">
                <div class="modal-header">
                    <h3 class="modal-title" id="h5-modal-title">Warning</h3>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">${paramAlert}</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
`)
}

//hàm hiển thị cảnh báo khi không thỏa điều kiện validate dùng modal
const successModal = (paramAlert) => {
    return (`
    <div id="success-contact-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content bg-success">
                <div class="modal-header">
                    <h3 class="modal-title" id="h5-modal-title">Success</h3>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">${paramAlert}</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-success-confirm">OK</button>
                </div>
            </div>
        </div>
    </div>
`)
}

//hàm hiện cảnh báo khi không thỏa vaildate
export const warningModalShow = (paramAlert) => {
    $('#warning-modal').empty()
    $('#warning-modal').append(warningModal(paramAlert))
    $('#warning-contact-modal').modal('show')
}

//hàm hiện thông báo thành công khi không thỏa vaildate
export const successModalShow = (paramAlert) => {
    $('#success-modal').empty()
    $('#success-modal').append(successModal(paramAlert))
    $('#success-modal').on('click', '.btn-success-confirm', () => {
        location.reload()
    })
    $('#success-contact-modal').modal('show')
}



export default addContactModalToHtml