/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

import { gCONTACT_URL, gId } from "../constants.js";
import { warningModalShow, successModalShow } from "./contactModal.js";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm xử lý sự kiện ấn nút thêm mớiContact trên modal
const onBtnCreateContact = () => {
    //B1: thu thập dữ liệu
    var vContactObj;
    vContactObj = getNewContact()
    //B2:validate dữ liệu
    if (validateContact(vContactObj)) {
        $('#warning-modal').empty()
        console.log(vContactObj);
        //B3: call api để thêm mớiContact
        callAPIToCreateContact(vContactObj)
    }
}

//hàm xử lý sự kiện ấn nút sửaContact trên modal
const onBtnUpdateContact = () => {
    console.log(gId);
    //B1: thu thập thông tin và điền vào input
    var vContactObj = getUpdateContact()
    console.log(vContactObj);
    if (validateContact(vContactObj)) {
        callAPIToUpdateContact(vContactObj)
    }
}

//hàm xử lý sự kiện ấn nút xóa Contact trên modal
const onBtnDeleteContact = () => {
    $.ajax({
        url: gCONTACT_URL + '/' + gId,
        type: 'DELETE',
        success: () => successModalShow('Delete Contact Sucessful'),
        error: err => console.log(err)
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// B1: hàm thu thập thông tin fill vào input của xem chi tiếtContact
const getUpdateContact = () => {
    return (
        {
            email: $('#inp-email').val()
        }
    )
}

//B1: hàm thu thập thông tin tạo mớiContact
const getNewContact = () => {
    return (
        {
            email: $('#inp-new-email').val()
        }
    )
}

//B2: hàm validate thông tin tạo mới từContact
const validateContact = (paramContact) => {
    if (!paramContact.email || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(paramContact.email))) {
        warningModalShow(`Please enter your correct email`)
        return false
    }
    return true
}

//B3: call api để sửaContact
const callAPIToUpdateContact = (paramContact) => {
    $.ajax({
        url: gCONTACT_URL + '/' + gId,
        type: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramContact),
        success: () => successModalShow('Update Contact Successful'),
        error: err => console.log(err)
    })
}

//B3: call api để thêm mớiContact
const callAPIToCreateContact = (paramContact) => {
    $.ajax({
        url: gCONTACT_URL,
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramContact),
        success: () => successModalShow('Create New Contact Successful'),
        error: err => console.log(err)
    })
}


export { onBtnCreateContact, onBtnDeleteContact, onBtnUpdateContact }