/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
import { getDetailFromIcon } from "../constants.js";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
const onIconEditClick = (paramIcon, paramTable) => {
    var vDetail = getDetailFromIcon(paramIcon, paramTable)
    $('#select-status').val(vDetail.status)
    $('#inp-name').val(vDetail.fullName)
    $('#inp-phone').val(vDetail.phone)
    $('#inp-created-at').val(vDetail.createdAt)
    $('#inp-updated-at').val(vDetail.updatedAt)
    $('#inp-email').val(vDetail.email)
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/


export { onIconEditClick }