import addContactModalToHtml from "./modal/contactModal.js";
import { gCONTACT_URL, getDetailFromIcon, gContactTable } from "./constants.js";
import { onIconEditClick } from "./modal/iconEvent.js";
import { onBtnCreateContact, onBtnDeleteContact, onBtnUpdateContact } from "./modal/contactEvent.js";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    "use strict";

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    //gọi hàm khi tải trang
    onLoadingPage()
    //gán sự kiện ấn nút thêm mới Contact
    $('#btn-add-contact').on('click', () => {
        $('#create-contact-modal').modal('show')
    })

    //gán sự kiện ấn nút xem chi tiết Contact
    $('#tbody-contacts').on('click', '.edit-contact', function () {
        $('#update-contact-modal').modal('show')
        onIconEditClick(this, gContactTable)
    })

    //gán sự kiện ấn nút xóa thông tin Contact
    $('#tbody-contacts').on('click', '.delete-contact', function () {
        $('#delete-contact-modal').modal('show')
        getDetailFromIcon(this, gContactTable)
    })

    //gán sự kiện ấn nút thêm mới Contact trên modal
    $('#btn-create-contact').on('click', function () {
        onBtnCreateContact()
    })
    //gán sự kiện sửa Contact trên modal
    $('#btn-update-contact').on('click', function () {
        onBtnUpdateContact()
    })
    //gán sự kiện xóa Contact trên modal
    $('#btn-delete-contact').on('click', function () {
        onBtnDeleteContact()
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm tải trang và load thông tin vào bảng
    function onLoadingPage() {
        // //call API Contact và fill vào table
        callAPIContacts()
        // add Modal vào HTML
        addContactModalToHtml()
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //hàm call API Contact để lấy thông tin vào table
    function callAPIContacts() {
        $.ajax({
            url: gCONTACT_URL,
            type: 'GET',
            async: false,
            success: res => {
                console.log(res.data);
                drawTable(res.data)
            },
            error: err => console.log(err)
        })
    }

    //hàm vẽ bảng từ data API Contact trả về
    function drawTable(paramRes) {
        gContactTable.clear()
        gContactTable.rows.add(paramRes)
        gContactTable.draw()
    }

});