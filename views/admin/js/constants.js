var gSTT = 0
var gId;

const gUserTable = $('#table-users').DataTable({
    columns: [
        { data: '_id' },
        { data: 'fullName' },
        { data: 'phone' },
        { data: 'status' },
        { data: 'action' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: () => {
                gSTT++
                return gSTT
            }
        },
        {
            targets: 4,
            className: "text-center",
            defaultContent: `
            <img class="edit-user" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="View Detail">
            <img class="delete-user" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Delete">
            `
        }
    ]
})

const gContactTable = $('#table-contacts').DataTable({
    columns: [
        { data: '_id' },
        { data: 'email' },
        { data: 'action' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: () => {
                gSTT++
                return gSTT
            }
        },
        {
            targets: 2,
            className: "text-center",
            defaultContent: `
            <img class="edit-contact" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="View Detail">
            <img class="delete-contact" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Delete">
            `
        }
    ]
})
const gUSER_URL = "http://localhost:8000/users";
const gCONTACT_URL = "http://localhost:8000/contacts";

//hàm lấy thông tin của dòng khi ấn nút sửa hoặc xóa
const getDetailFromIcon = (paramIcon, paramTable) => {
    var vRow = $(paramIcon).parents('tr')
    var vDetail = paramTable.row(vRow).data()
    gId = vDetail._id
    return vDetail
}
export { gUserTable, getDetailFromIcon, gUSER_URL, gId, gCONTACT_URL, gContactTable }