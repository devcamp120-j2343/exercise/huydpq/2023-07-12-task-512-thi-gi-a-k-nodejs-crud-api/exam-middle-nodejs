import addUserModalToHtml from "./modal/modal.js";
import { gUSER_URL, getDetailFromIcon, gUserTable } from "./constants.js";
import { onBtnCreateUser, onBtnDeleteUser, onBtnUpdateUser } from "./modal/event.js";
import { onIconEditClick } from "./modal/iconEvent.js";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    "use strict";

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    //gọi hàm khi tải trang
    onLoadingPage()
    //gán sự kiện ấn nút thêm mới user
    $('#btn-add-user').on('click', () => {
        $('#create-user-modal').modal('show')
    })

    //gán sự kiện ấn nút xem chi tiết user
    $('#tbody-users').on('click', '.edit-user', function () {
        $('#update-user-modal').modal('show')
        onIconEditClick(this, gUserTable)
    })

    //gán sự kiện ấn nút xóa thông tin user
    $('#tbody-users').on('click', '.delete-user', function () {
        $('#delete-user-modal').modal('show')
        getDetailFromIcon(this, gUserTable)
    })

    //gán sự kiện ấn nút thêm mới user trên modal
    $('#btn-create-user').on('click', function () {
        onBtnCreateUser()
    })
    //gán sự kiện sửa user trên modal
    $('#btn-update-user').on('click', function () {
        onBtnUpdateUser()
    })
    //gán sự kiện xóa user trên modal
    $('#btn-delete-user').on('click', function () {
        onBtnDeleteUser()
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm tải trang và load thông tin vào bảng
    function onLoadingPage() {
        // //call API user và fill vào table
        callAPIusers()
        // add Modal vào HTML
        addUserModalToHtml()
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //hàm call API user để lấy thông tin vào table
    function callAPIusers() {
        $.ajax({
            url: gUSER_URL,
            type: 'GET',
            async: false,
            success: res => {
                console.log(res.data);
                drawTable(res.data)
            },
            error: err => console.log(err)
        })
    }

    //hàm vẽ bảng từ data API user trả về
    function drawTable(paramRes) {
        gUserTable.clear()
        gUserTable.rows.add(paramRes)
        gUserTable.draw()
    }

});